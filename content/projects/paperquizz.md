---
title: Paperquizz
language: ['Django']
description: "Création d'un site web moderne pour les professeurs de l'IUT d'Orléans leurs permettant de créer des QCM papiers auto-corrigés."
image: ./images/paperquizz/paperquizz-index.png
link: 
---
Projet de développement d'un site web moderne pour les professeurs de l'IUT d'Orléans leurs permettant de créer des QCM papiers auto-corrigés.