---
title: Ecole IOT 
language: ['WordPress']
description: "Création d'un site web pour une nouvelle formation à Orléans sous WordPress"
image: ./images/ecoleiot/homepage.png
link: "https://ecoleiot.fr"
---
Ecole IOT est un site réalisé au sein de l'entreprise Krea'Lab à Orléans.

Ce site a pour but de présenter une nouvelle formation d'architecte IOT à Orléans.