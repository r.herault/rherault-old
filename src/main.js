// This is the main.js file. Import global CSS and scripts here.
// The Client API can be used here. Learn more: gridsome.org/docs/client-api
import '~/assets/styles.scss'
import DefaultLayout from '~/layouts/Default.vue'

export default function (Vue, { router, head, isClient }) {
  // Set default layout as a global component
  Vue.component('Layout', DefaultLayout)
  head.htmlAttrs = { lang: 'fr' }
  head.meta.push(
    {
      name: 'description',
      content: 'Romain HERAULT - Site personnel, Quelques outils libres mis à disposition'
    },
    // Facebook
    {
      property: 'og:site_name',
      content: 'rherault'
    },
    {
      property: 'og:title',
      content: 'rherault'
    },
    {
      property: 'og:type',
      content: 'website'
    },
    {
      property: 'og:description',
      content: 'Romain HERAULT - Site personnel, Quelques outils libres mis à disposition'
    },
    {
      property: 'og:url',
      content: 'https://rherault.fr'
    },
    {
      property: 'og:locale',
      content: 'fr_FR'
    },
    // Twitter
    {
      name: 'twitter:card',
      content: 'summary'
    },
    {
      name: 'twitter:creator',
      content: '@romaixn'
    },
    {
      name: 'twitter:url',
      content: 'https://rherault.fr'
    },
    {
      name: 'twitter:title',
      content: 'rherault'
    },
    {
      name: 'twitter:description',
      content: 'Romain HERAULT - Site personnel, Quelques outils libres mis à disposition'
    },
    {
      name: 'twitter:dnt',
      content: 'on'
    },
    // Google
    {
      name: 'google',
      content: 'notranslate'
    },
    {
      name: 'robots',
      content: 'index,follow'
    },
    {
      name: 'googlebot',
      content: 'index,follow'
    },
    {
      name: 'rating',
      content: 'General'
    },
    {
      name: 'referrer',
      content: 'no-referrer'
    },
    {
      name: 'x-dns-prefetch-control',
      content: 'off'
    }
  )

  head.link.push(
    // Contact
    {
      rel: 'me',
      href: 'mailto:romain.herault99@gmail.com'
    },
    // DNS Optimization
    {
      rel: 'dns-prefetch',
      href: '//rherault.fr/'
    },
    {
      rel: 'preconnect',
      href: 'https://rherault.fr/'
    },
    {
      rel: 'prefetch',
      href: 'https://rherault.fr/'
    },
    {
      rel: 'prerender',
      href: 'https://rherault.fr/'
    }
  )
}
